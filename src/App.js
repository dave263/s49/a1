import { useState } from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import { UserProvider } from './UserContext'

import Home from './pages/Home'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import ErrorPage from './pages/ErrorPage'

import AppNavbar from './components/AppNavbar'
import Footer from './components/Footer'

function App() {

  const [user, setUser] = useState({
      id: 123,
      isAdmin: true
  })

  return(
    <UserProvider value={{user, setUser}} >
      <BrowserRouter>
        <AppNavbar/>
        <Routes>
          <Route path="/" element={ <Home/> } />
          <Route path="/courses" element={ <Courses/> } />
          <Route path="/register" element={ <Register/> } />
          <Route path="/login" element={ <Login/> } />
          <Route path="*" element={ <ErrorPage/> } />
        </Routes>
        <Footer/>
      </BrowserRouter>
    </UserProvider>

  )
}

export default App;
